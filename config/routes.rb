BluewhaleSocialAdmin::Engine.routes.draw do

	root 'users#index'
	resources :dashboard
	resources :users do 
		get :change_password
		put :update_password
	end
	resources :posts do 
		get :assets
		get :video
	end
	resources :comments
 
end

class AdminConfigGenerator < Rails::Generators::NamedBase
  source_root File.expand_path('../templates', __FILE__)

  def social_admin_initialize
    copy_file "admin.yml", "config/admin.yml"
    social_admin_route = 'mount BluewhaleSocialAdmin::Engine => "/admin"'
    route social_admin_route
  end
end
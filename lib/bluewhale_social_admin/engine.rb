module BluewhaleSocialAdmin
  class Engine < ::Rails::Engine
    isolate_namespace BluewhaleSocialAdmin
  end
end

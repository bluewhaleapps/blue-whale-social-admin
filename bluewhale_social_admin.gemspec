$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "bluewhale_social_admin/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "bluewhale_social_admin"
  s.version     = BluewhaleSocialAdmin::VERSION
  s.authors     = ["Mike Jaffe"]
  s.email       = ["mike@bluewhaleapps.com"]
  s.homepage    = "https://github.com/bluewhaleinc/"
  s.summary     = "Provides a framework for administering the social api"
  s.description = "Provides a framework for administering the social api"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.0.0"

  s.add_development_dependency "pg"
end

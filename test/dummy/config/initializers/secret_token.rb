# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Dummy::Application.config.secret_key_base = 'e2f5f263a841622b614d715bc8b104636957229211f979cd4c8a71e18622c03279a778ca7f0e1d21a70b9b9b1d81d8ac7a4d87d030696ef1206c9789a4dee0f9'

module BluewhaleSocialAdmin
	require 'csv'
  class ApplicationController < ActionController::Base
	before_filter :authenticate

      def as_csv(obj,cols)
        CSV.generate do |csv|
          csv << cols
          obj.each do |item|
            csv << item.attributes.values_at(*cols)
          end
        end
      end


 

	private
    def authenticate
      creds = YAML.load_file(Rails.root.to_s + '/config/admin.yml')
      authenticate_or_request_with_http_basic do |username, password|
        session[:admin] = true
        username ==  creds["username"] && password ==  creds["password"]
      end
    end

  end
end

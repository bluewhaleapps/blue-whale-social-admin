require_dependency "bluewhale_social_admin/application_controller"

module BluewhaleSocialAdmin
  class PostsController < ApplicationController

	def index
		params[:search] ||= ""
		sort = params[:sort].present? ? "#{params[:sort]} #{params[:dir]}" : "created_at desc"
		search = params[:search].eql?("") ? ["1=1"] : ["lower(users.first_name) like :search or lower(users.last_name) like :search or lower(post_text) like :search or username = :search ",{:search => "#{params[:search].downcase}%"}]
		search = "users.id = #{params[:user_id]}" if params[:user_id].present?
		search = "posts.id = #{params[:post_id]}" if params[:post_id].present?
		@posts = Post.unscoped.where(search)
					.joins("left join (select count(*) comment_count,object_type_id from comments where object_type ='post' group by object_type_id) c on c.object_type_id = posts.id")
					.joins("left join (select count(*) like_count,object_type_id from likes where object_type ='post' group by object_type_id) l on l.object_type_id = posts.id")
					.joins(:user).page(params[:page]).per(params[:per]).order(sort)
	end

	def show
		@post = Post.unscoped.find(params[:id])
	end

	def edit
		@post = Post.unscoped.find(params[:id])
	end

	def update
		@post = Post.unscoped.find(params[:id])
		if @post.update_attributes(post_params)
			flash.now[:result] = "Post updated"
		else
			flash.now[:error] = @post.errors.full_messages
		end
		render :edit
	end

	def assets
		@asset =  Asset.find(params[:post_id]) 
	end

	def video
		@video = Asset.find(params[:post_id]) 
	end

	def destroy
		@post = Post.unscoped.find(params[:id])
		@post.destroy
		redirect_to posts_path
	end


  	private
  	def post_params
  		params.require(:post).permit!
  	end

  end
end

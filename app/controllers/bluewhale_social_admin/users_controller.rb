require_dependency "bluewhale_social_admin/application_controller"

module BluewhaleSocialAdmin
  class UsersController < ApplicationController

    def index
      params[:search] ||= ""
      sort = params[:sort].present? ? "#{params[:sort]} #{params[:dir]}" : "created_at desc"
      search = params[:search].eql?("") ? ["1=1"] : ["lower(users.first_name) like :search or lower(users.last_name) like :search or lower(username) = :search ", {:search => "#{params[:search].downcase}%"}]
      params[:page] ||= 1
      params[:per] ||= 25


      respond_to do |format|
        format.html {
          @users = User.unscoped.joins("left join (select count(*) post_count,user_id from posts group by user_id) p on p.user_id = users.id").where(search).order(sort).page(params[:page]).per(params[:per])
        }
        format.csv {
          @users = User.unscoped.where(search).order(sort)
          send_data self.as_csv(@users, ["username", "email", "facebook_id", "first_name", "last_name", "address1", "address2", "city", "state", "zip", "is_deleted"])
        }
      end

    end

    def new
      @user = User.new
    end

    def edit
      @user = User.unscoped.find(params[:id])
    end

    def create
      @user = User.new(user_params)
      if @user.save
        flash[:result] = "User created"
        redirect_to users_url
      else
        flash.now[:error] = @user.errors.full_messages
        render 'new'
      end
    end

    def update
      @user = User.unscoped.find(params[:id])
      if @user.update_attributes(user_params)
        @user.update_attribute("authentication_token", nil) if @user.is_deleted
        flash.now[:result] = "User updated"
      else
        flash.now[:error] = @user.errors.full_messages
      end
      render :edit
    end

    def destroy
      User.unscoped.find(params[:id]).destroy
      redirect_to users_path
    end

    def change_password
      @user = User.unscoped.find(params[:user_id])
    end

    def update_password
      @user = User.unscoped.find(params[:user_id])
      if @user.update_attributes(user_params)
        @user.pass_gen
        @user.save!(:validate => false)
        flash.now[:result] = "Password updated"
      else
        flash.now[:error] = @user.errors.full_messages
      end
      render :change_password
    end


    private
    def user_params
      params.require(:user).permit!
    end
  end
end

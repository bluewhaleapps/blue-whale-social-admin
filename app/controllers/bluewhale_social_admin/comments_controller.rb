require_dependency "bluewhale_social_admin/application_controller"

module BluewhaleSocialAdmin
  class CommentsController < ApplicationController

  	def index
  		params[:object_type] ||= "post"
  		search = params[:search].blank? ? ["1=1"] : ["lower(users.first_name) like :search or lower(users.last_name) like :search or lower(comments.comment) like :search or lower(users.username) = :search ",{:search => "#{params[:search].downcase}%"}]
  		sort = params[:sort].present? ? "#{params[:sort]} #{params[:dir]}" : "created_at desc"
  		search = ["object_type_id = ?",params[:object_type_id]]  if params[:object_type_id].present?
  		@comments = Comment.unscoped.joins(:user).where(search).where(["object_type = ?",params[:object_type]]).page(params[:page]).per(params[:per]).order(sort)
  	end

  	def edit
  		@comment = Comment.unscoped.find(params[:id])
  	end

	def update
		@comment = Comment.unscoped.find(params[:id])
		if @comment.update_attributes(comment_params)
			flash.now[:result] = "Comment updated"
		else
			flash.now[:error] = @comment.errors.full_messages
		end
		render :edit
	end


  	private
  	def comment_params
  		params.require(:comment).permit!
  	end

  end
end
